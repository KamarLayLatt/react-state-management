'use client'
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement,incrementByAmount } from '../../redux/features/counter';
import { RootState } from '../../redux/store'; 
// import { StoreProvider } from '../StoreProvider';

export default function Page() {
  const count = useSelector((state: RootState) => state.counter.value);
  const dispatch = useDispatch();

  return (
    // <StoreProvider>
    <div>
      <h1>Count: {count}</h1>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
    </div>
      
    // </StoreProvider>
  );
  }